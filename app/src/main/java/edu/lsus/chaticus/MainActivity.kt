package edu.lsus.chaticus

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import okhttp3.RequestBody
import org.json.JSONObject



class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //--- Calls method "listWork()" to GET a list of all current conversations -----------------
        listWork()

        //--- Value declaration/instantiation ------------------------------------------------------
        val editText: EditText = findViewById(R.id.editMessage)
        val button: Button = findViewById(R.id.sendMessage)
        val actionBar = getActionBar()

        //--- Clicking the button pulls data from the EditText and posts it as a new conversation --
        button.setOnClickListener {
            val messageString: String = "{ \"title\": \"" + editText.getText().toString() + "\" }"
            val messageJSON = JSONObject(messageString)
            val body: RequestBody = RequestBody
                    .create(okhttp3.MediaType.parse("application/json; charset=utf-8"), messageJSON.toString())
            app.api.postConversation(body).enqueue(object : Callback<MessageDto> {
                override fun onResponse(call: Call<MessageDto>?, response: Response<MessageDto>?) {
                    listWork() // Calls listWork again to reflect changes
                    editText.setText("") // Clears the edit text
                }

                override fun onFailure(call: Call<MessageDto>?, t: Throwable?) {

                }
            })
        }
    }

    //--- I encapsulated GET for conversations so as to be called for different situations ---------
    fun listWork() {

        val listOfMessages: ListView = findViewById<ListView>(R.id.list_of_messages)

        if (!app.session.hasValidCredentials()) {
            app.login(this)
        }
        else {
            app.api.getMessages().enqueue(object : Callback<List<MessageDto>> {
                override fun onResponse(call: Call<List<MessageDto>>?, response: Response<List<MessageDto>>?) {
                    val messages : List<MessageDto>? = response?.body()
                    val size: Int = messages!!.size
                    val messagesArray = Array(size, { i -> messages!![i].content })
                    // I wrote a custom ListView adapter to inflate and populate the ListView
                    listOfMessages.adapter = CustomAdapter(this@MainActivity, size, messagesArray)
                    listOfMessages.setSelection(size)


                }

                override fun onFailure(call: Call<List<MessageDto>>?, t: Throwable?) {

                }
            })
        }
    }

    //--- Creating the options menu in the Action Bar ----------------------------------------------
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return super.onCreateOptionsMenu(menu)

    }

    //--- Refreshes the conversation feed ----------------------------------------------------------
    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.refresh -> {45
            listWork()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }


    //--- Custom Adapter Subclass ------------------------------------------------------------------
    private class CustomAdapter(context: Context, rows: Int, conversationArray: Array<String>): BaseAdapter() {
        private val mContext: Context
        private val mRows: Int
        private val mConversationArray: Array<String>
        init {
            mContext = context
            mRows = rows
            mConversationArray = conversationArray
        }

        override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
            val layoutInflater = LayoutInflater.from(mContext)
            val conversationRow = layoutInflater.inflate(R.layout.conversation_row, viewGroup, false)
            val idTextView = conversationRow.findViewById<TextView>(R.id.convoId)
            val titleTextView = conversationRow.findViewById<TextView>(R.id.convoTitle)
            val id: Int = position + 1
            idTextView.text = "Conversation ID: " + id.toString()
            titleTextView.text = "\"" + mConversationArray[position] + "\""

            return conversationRow
        }

        override fun getItem(position: Int): Any {
            return "TEST"
        }

        override fun getCount(): Int {
            return mRows
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }
    }
}