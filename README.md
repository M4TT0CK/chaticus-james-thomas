**CSC 490 - Mobile Application Development**

**Final Exam**

**Summary:** Build an Android application that functions as a messaging app.

**Due:** Monday, 4/30/2018 at 11:59:59 PM

**Rubric:** 300 Points

1. (30 pts) When I launch the app, I should be presented with the ability to login / sign up with either my Google account or with a username and password.
2. (45 pts) After signing in, I should be shown a list of conversations that I am a part of.
2. (15 pts) When I am on the conversation list page, I should be able to click a button to create a new conversation.
2. (20 pts) When I click the button to create a new conversation, I should be able to enter the email address(es) of the particapant(s).
2. (20 pts) After confirming the creation of the conversation, I should be taken to a page showing the conversation.
2. (20 pts) When I am on the conversation list page, I should be able to go to the details for the conversation.
2. (20 pts) When I am on the details page for a conversation, I should see a list of messages on the page.
2. (20 pts) When I am on the details page for a conversation, I should be able to clearly identify the sender of a particular message.
2. (20 pts) When I am on the conversation details page, I should be able to add a message to conversation.
2. (30 pts) I should be notified when a message is sent to a conversation that I am a member of.
3. (30 pts) Provide a CHANGES.md file in the root of your project that details the design decisions that you make, suggests alternatives that you considered, and explains your reasoning for choosing what you did.
4. (30 pts) Impress me.

**Submission:** You will submit your project by creating a git repository and pushing it to your BitBucket repository (that you have granted me access) by the due date and time. 