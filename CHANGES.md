**CSC 490 - Mobile Application Development**

**Final Exam - Changes**

**Design Decisions:** Most of my design decisions were based on either time constraitns or working around the API, or some combination therein. For instance, since I wasn't able to post messages to individual conversations, I just displayed a list of all conversations,
listing their ID number and the title, using the title in lieu of a message body.

**Alternatives Considered:** I considered using a RecyclerView instead of the ListView I ended up using.

**Rationale:** I mostly used the ListView in place of a RecyclerView because the examples I saw were more immediately applicable.